// @flow
const API = {
  message: 'API Documentation',
  api: [
    {
      url: '/api/todos',
      method: 'GET',
      message: 'List of Todos'
    },
    {
      url: '/api/todos',
      method: 'POST',
      message: 'Add a Todo'
    },
    {
      url: '/api/todos/:todoId',
      method: 'GET',
      message: 'Get a Todo'
    },
    {
      url: '/api/todos/:todoId',
      method: 'PUT',
      message: 'Update a Todo'
    },
    {
      url: '/api/todos/:todoId',
      method: 'DELETE',
      message: 'Delete a Todo'
    }
  ]
};
export default API;
