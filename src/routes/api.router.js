import Router from 'koa-router';
import todoRouter from './todo.router';
import API from './api.doc';

const apiRouter = Router({ prefix: '/api' });
apiRouter
  .get('/', ctx => {
    ctx.body = {
      message: 'API Documentation',
      API
    };
  })
  .use(todoRouter.routes(), todoRouter.allowedMethods());

export default apiRouter;
