// @flow
import Router from 'koa-router';
import { todoController } from '../controllers';

const todosRouter = new Router({ prefix: '/todos' });
const todoRouter = new Router({ prefix: '/:todoId' });

// route:api/todos
todosRouter.get('/', todoController.index).post('/', todoController.create);

// route:api/todos/:id
todoRouter
  .get('/', todoController.show)
  .put('/', todoController.update)
  .delete('/', todoController.delete);
todosRouter.use(todoRouter.routes(), todoRouter.allowedMethods());

export default todosRouter;
