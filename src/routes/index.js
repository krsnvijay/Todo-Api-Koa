// @flow
import Router from 'koa-router';
import send from 'koa-send';
import apiRouter from './api.router';

const rootRouter = Router();

rootRouter
  .get('/', async ctx => {
    await send(ctx, ctx.path, { root: `${__dirname}/public` });
  })
  .use(apiRouter.routes(), apiRouter.allowedMethods());

export default rootRouter;
