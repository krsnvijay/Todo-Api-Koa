// @flow
import type { Context } from 'koa';
import { Todo } from '../models';

const todoController = {
  async index(ctx: Context) {
    try {
      // Get list of todos
      const result = await Todo.find();
      ctx.body = result;
    } catch (error) {
      ctx.throw(400, 'INVALID_DATA');
    }
  },
  async show(ctx: Context) {
    const { params } = ctx;
    if (!params.todoId) ctx.throw(400, 'INVALID_DATA');

    try {
      // Find and show todo
      const todo = await Todo.findById(params.todoId);
      ctx.body = todo;
    } catch (error) {
      ctx.throw(400, 'INVALID_DATA');
    }
  },
  async create(ctx: Context) {
    const { body } = ctx.request;
    try {
      // create Todo
      const todo = await Todo.create(body);
      ctx.body = todo;
      ctx.status = 201;
    } catch (error) {
      ctx.throw(400, 'INVALID_DATA');
    }
  },
  async update(ctx: Context) {
    const { params } = ctx;
    const { body } = ctx.request;

    // Make sure they've specified a todo
    if (!params.todoId) ctx.throw(400, 'INVALID_DATA');

    try {
      const todo = await Todo.findOneAndUpdate({ _id: params.todoId }, body, {
        new: true
      });
      ctx.body = todo;
    } catch (error) {
      ctx.throw(400, 'INVALID_DATA');
    }
  },
  async delete(ctx: Context) {
    const { params } = ctx;
    if (!params.todoId) ctx.throw(400, 'INVALID_DATA');

    try {
      await Todo.findOneAndRemove({ _id: params.todoId });
      ctx.body = { message: 'SUCCESS' };
    } catch (error) {
      ctx.throw(400, 'INVALID_DATA');
    }
  }
};
export default todoController;
