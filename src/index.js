// @flow
import Koa from 'koa';
import bodyParser from 'koa-bodyparser';
import serve from 'koa-static';
import router from './routes';

const app = new Koa();
const port = process.env.PORT || 3000;
app.use(bodyParser());
app.use(serve(`${__dirname}/public`));
app.use(router.routes()).use(router.allowedMethods());
app.listen(port);
