// @flow
import mongoose from 'mongoose';
import Todo from './todo.model';

mongoose.set('debug', true);
mongoose.connect('mongodb://todo:1234@ds131329.mlab.com:31329/not-a-hero');

mongoose.Promise = Promise;
export { Todo };
export default { Todo };
